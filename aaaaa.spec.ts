export const AffixMapping: Record<number, number> = {
  // Level 2
  [Affix.Fortified]: MythicPlusAffix.Fortified,
  [Affix.Tyrannical]: MythicPlusAffix.Tyrannical,

  /*
  // DF S1
  // Level 4
  [Affix.Bolstering]: MythicPlusAffix.Bolstering,
  [Affix.Bursting]: MythicPlusAffix.Bursting,
  [Affix.Inspiring]: MythicPlusAffix.Inspiring,
  [Affix.Raging]: MythicPlusAffix.Raging,
  [Affix.Sanguine]: MythicPlusAffix.Sanguine,
  [Affix.Spiteful]: MythicPlusAffix.Spiteful,

  // Level 7
  [Affix.Explosive]: MythicPlusAffix.Explosive,
  [Affix.Grievous]: MythicPlusAffix.Grievous,
  [Affix.Quaking]: MythicPlusAffix.Quaking,
  [Affix.Storming]: MythicPlusAffix.Storming,
  [Affix.Volcanic]: MythicPlusAffix.Volcanic,

  // Level 10
  [Affix.Thundering]: MythicPlusAffix.Thundering,
  */

  // DF S2
  // Level 7
  [Affix.Afflicted]: MythicPlusAffix.Afflicted,
  [Affix.Incorporeal]: MythicPlusAffix.Incorporeal,
  [Affix.Volcanic]: MythicPlusAffix.Volcanic,
  [Affix.Entangling]: MythicPlusAffix.Entangling,
  [Affix.Storming]: MythicPlusAffix.Storming,

  // Level 14
  [Affix.Spiteful]: MythicPlusAffix.Spiteful,
  [Affix.Raging]: MythicPlusAffix.Raging,
  [Affix.Bursting]: MythicPlusAffix.Bursting,
  [Affix.Bolstering]: MythicPlusAffix.Bolstering,
  [Affix.Sanguine]: MythicPlusAffix.Sanguine,
};
